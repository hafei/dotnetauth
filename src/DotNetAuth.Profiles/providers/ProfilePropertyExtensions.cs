using System.Linq;

namespace DotNetAuth.Profiles
{
    public static class ProfilePropertyExtensions
    {
        public static ProfileProperty Find(this ProfileProperty[] supportedProperties, ProfileProperty requiredProperty)
        {
            return supportedProperties.SingleOrDefault(p => p.PropertyName == requiredProperty.PropertyName);
        }
        public static ProfileProperty[] Find(this ProfileProperty[] supportedProperties, ProfileProperty[] requiredProperties)
        {
            return requiredProperties.Select(rp => supportedProperties.Find(rp)).Where(sp => sp != null).ToArray();
        }
        public static string GetFields(this ProfileProperty[] supportedProperties, ProfileProperty[] requiredProperties, string separator = ",")
        {
            if (requiredProperties == null)
                return "";
            return string.Join(separator, supportedProperties.Find(requiredProperties).Select(p => p.EndpointSettings).Distinct().ToArray());
        }
        public static string GetScope(this ProfileProperty[] supportedProperties, ProfileProperty[] requiredProperties, string separator = ",")
        {
            if (requiredProperties == null)
                return "";
            return string.Join(separator, supportedProperties.Find(requiredProperties).Select(p => p.RequiredScope).Distinct().ToArray());
        }
    }
}
