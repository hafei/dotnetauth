﻿using System;
using System.Web.Mvc;

namespace DotNetAuth.Profiles.Sample.Controllers
{
    public class ProfileController : Controller
    {
        readonly ProfileProperty[] requiredProperties = new[] { ProfileProperty.Email, ProfileProperty.DisplayName, ProfileProperty.UniqueID, ProfileProperty.DisplayName };
        // GET: /Profile/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public RedirectResult Login(string providerName)
        {
            var userProcessUri = Url.Action("Process" + providerName, "Profile", null, protocol: Request.Url.Scheme);
            var provider = LoginProvider.Get(providerName);
            var authorizationUrl = Profiles.Login.GetAuthenticationUri(provider, new Uri(userProcessUri), new DefaultLoginStateManager(Session), requiredProperties);
            authorizationUrl.Wait();
            return Redirect(authorizationUrl.Result.AbsoluteUri);
        }
        // GET: /Process
        [HttpGet]
        public ActionResult Callback(string providerName)
        {
            var userProcessUri = Url.Action("Process" + providerName, "Profile", null, protocol: Request.Url.Scheme);
            var provider = LoginProvider.Get(providerName);
            var profile = Profiles.Login.GetProfile(provider, Request.Url, userProcessUri, new DefaultLoginStateManager(Session), requiredProperties);
            profile.Wait();
            return Content(profile.Result.ToString());
        }
    }
}
